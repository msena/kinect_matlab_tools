classdef (Abstract) IStream < handle
%% (Abstract) Classes for accessing live or recorded data from 3D cameras
% - start, stop, and update sensor or file streams
% - access raw image, pointcloud, and skeleton data
%
% see also stream.Sensor, stream.OniFile

% TODO
% - relative XML_PATH
% - daq function descriptions
% - check skel data for all 3 camera orientations

%% properties
properties (Constant, Hidden)
    IM_SZ = [640 480];  % size of images coming from Mex.mxNi*() calls
end
properties (SetAccess = protected)
    oniPath             % absolute path of .oni file to open or record
    camOri = 'H'        % camera orientation ({'H'}|'L'|'R')
end
properties (SetAccess = protected, Hidden)
    streamID_            % input sensor stream ID
end

%% methods
methods (Abstract)
    next(this)          % step to next frame
end
methods
%% sensor control
    function delete(this)
        %% Stop the sensor
        Mex.mxNiDeleteContext(this.streamID_)
        this.streamID_ = [];
        fprintf(2, 'STOPPED STREAM\n') %#ok<PRTCAL>
    end

%% data acquisition
    function im = getInfrared(this)
        im = rotImage( Mex.mxNiInfrared(this.streamID_),...
            this.camOri );
    end

    function im = getColor(this)
        im = rotImage( Mex.mxNiPhoto(this.streamID_),...
            this.camOri );
    end

    function im = getDepth(this)
        im = rotImage( Mex.mxNiDepth(this.streamID_),...
            this.camOri );
    end

    function p_xyz = getPoints(this)
        p_xyz = rotPoints( Mex.mxNiDepthRealWorld_Pout(this.streamID_),...
            this.camOri );
    end

    function [p_xyz, conf] = getSkel3D(this)
        %% get (nUsers*15)-by-3 camera coordinates of skeleton joints
        skMat = Mex.mxNiSkeleton(this.streamID_);
        isUser = skMat(:,1) ~= 0;       % ignore empty rows
        conf = skMat(isUser, 2);
        temp = skMat(isUser, 3:5);      % i,j coords
        p_xyz = rotPoints(temp, this.camOri);
    end

    function [p_ij0, conf] = getSkel2D(this)
        %% get (nUsers*15)-by-3 pixel coordinates of skeleton joints
        % Note: the 3rd column are zeros
        skMat = Mex.mxNiSkeleton(this.streamID_);
        isUser = skMat(:,1) ~= 0;       % ignore empty rows
        conf = skMat(isUser, 2);
        temp = skMat(isUser, 6:7);      % x,y,z coords
        p_ij = rotSkel2D(temp, this.camOri, this.IM_SZ);
        p_ij0 = [p_ij, zeros(size(p_ij,1), 1)];
    end

%% get/set functions (input validation)
    function set.camOri(this, camOri)
        if isempty(camOri)
            camOri = '';
        end
        assert(any(strcmpi(camOri, {'R','L','H',''})),...
            'camera orientation camOri must be "R", "L", "H" or ""');
        this.camOri = camOri;
    end
end
end

%% helper functions
    function im = rotImage(im, camOri)
        %% Transform image data according to camera view
        % Note that image data comes in transposed for indexing purposes in C++
        switch camOri
            case {'','h','H'}           % horizontal
                im = im';
            case {'r','R'}              % laser beneath camera
                im = fliplr(im);
            case {'l','L'}              % laser above camera
                im = flipud(im);
            otherwise
                error('camera orientation must be "R", "L", "H", or empty')
        end
    end

    function p_ij = rotSkel2D(p_ij, camOri, imSz)
        %% transform skel image coords according to camera orientation
        switch camOri
            case {'','h','H'}           % horizontal
                % do nothing
            case {'r','R'}              % laser beneath camera
                p_ij(:,2) = imSz(2) - p_ij(:,2) + 1;
                p_ij = fliplr(p_ij);
            case {'l','L'}              % laser above camera
                p_ij(:,1) = imSz(1) - p_ij(:,1) + 1;
                p_ij = fliplr(p_ij);            
            otherwise
                error('camera orientation must be "R", "L", "C", or empty')
        end
    end

    function p_xyz = rotPoints(p_xyz, camOri)
        %% transform 3D point data
        % try using hgtransform to rotate points instead
        T_CL = [-1 0 0; 0 0 1; 0 1 0];  % right-transforms camera to lab coords
        switch camOri
            case {'','h','H'}           % horizontal
                    p_xyz = p_xyz * T_CL;
            case {'r','R'}              % laser beneath camera
                p_xyz = p_xyz * T_CL * [0 0 -1;  0 1 0;  1 0 0];
            case {'l','L'}              % laser above camera
                p_xyz = p_xyz * T_CL * [0 0 1;  0 1 0;  -1 0 0];
            otherwise
                error('camera orientation must be "R", "L", "C", or empty')
        end
    end