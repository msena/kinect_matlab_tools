classdef Sensor < stream.IStream
%% Data stream from a live Kinect of Xtion 3D camera
%
%   S = stream.Sensor(camOri)
%
% see also stream.OniFile

% TODO:
% - help lines for data aq methods
% - relative XML_PATH
% - check that .oni file write so path specified

% NOTES:
% - uses Mex.mxNiCreateContext_nFramesOut()
% - uses original Mex.mxNiUpdateContext()
% - unlike OniFile Stream, the update is done internally for maximum speed
%       (OniFile Stream uses repeated calls to the next() method)

%% Properties
properties
    isRecording = false     % whether recording has started
end
properties (Constant)
    XML_PATH = ...          % sensor config: infrared, mirror off
        '+mex\XMLConfigs\SamplesConfigIR_mirrorOff.xml';
    N_REC_MAX = 9000;       % max number of frames recordable (9k=5min)
end
properties (SetAccess = private, Hidden)
    captureID_              % recorder handles
    tstampID_               % timestamp .txt file ID
    nCount_ = 0             % number of frames displayed (resets on recStart())
    tPrev_ = 0              % prevous timestamp (for FPS calc)
end

%% Methods
methods
    function this = Sensor(camOri)
    %% Set camera orientation if specified and start sensor
        if nargin > 0
            this.camOri = camOri;
        end
        this.streamID_ = Mex.mxNiCreateContext_nFramesOut(this.XML_PATH);            
    end

    function delete(this)
    %% stop if recording and call superclass destructor
    % NOTE: superclass detructor is automatically called in addition
        if ~isempty(this.captureID_)
            recStop(this);
            pause(0.2);       % extra time for the sensor
        end
    end

    function next(this)  % (implementation)
    %% Update stream and if recording, write timestamp to file
    % - stop recording if the max number of frames are exceeded
    % - display mean FPS as well
        Mex.mxNiUpdateContext(this.streamID_);
        tNow = Mex.hat;
        n = this.nCount_ + 1;
        this.nCount_ = n;

        % display average FPS every 30 frames
        if ~mod(n, 30)
            FPS = 30/(tNow - this.tPrev_);
            this.tPrev_ = tNow;
            fprintf('%.0f fps\n', FPS);
        end

        % write timestamp to file
        if this.isRecording
            fprintf(this.tstampID_, '%f\n', tNow);  % via hat.c
            if this.nCount_ == this.N_REC_MAX
                recStop(this)
            end
        end
    end

    function recStart(this, oniPath)
    %% Start recording data to .oni file, and timestamps to .txt file
        [fPath, fName] = fileparts(oniPath);
        assert(isempty(fPath) || exist(fPath, 'dir') == 7,...
            'Could not find directory:\n\t%s', fPath)
        oniPath = [fullfile(fPath, fName), '.oni'];  % ensure .oni extension            
        this.captureID_ = Mex.mxNiStartCapture(this.streamID_, oniPath);
        this.oniPath = oniPath;

        % create timestamp <fPath/fName>.txt file with write permissions
        tsPath = [fullfile(fPath, fName), '.txt'];
        this.tstampID_ = fopen(tsPath, 'w');

        this.isRecording = true;
        this.nCount_ = 0;           % reset counter
    end

    function recStop(this)
    %% Stop recording. Close .oni file and .txt tiemstamp file
    % - Stops and restarts stream since Mex.mxNiStopCapture does not stop capture            
        if this.isRecording
            % close timestamp file
            fclose(this.tstampID_);
            this.tstampID_ = [];

            % stop recording and restart sensor
            Mex.mxNiStopCapture(this.captureID_);
            this.captureID_ = [];
            fprintf(2, 'STOPPED RECORDING\n');
            fprintf('SAVED %d FRAMES TO:\n  %s\n\n', this.nCount_, this.oniPath);
            this.isRecording = false;

            Mex.mxNiDeleteContext(this.streamID_);
            this.streamID_ = Mex.mxNiCreateContext(this.XML_PATH);
            fprintf(2, 'RESTARTED STREAM\n');
        else
            warning('HAVEN''T STARTED RECORDING');
        end
    end 
end

end % classdef
