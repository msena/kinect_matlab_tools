classdef OniFile < stream.IStream
%% Data stream from a pre-recorded .oni file
%
%   S = stream.OniFile(oniPath)
%   S = stream.OniFile(oniPath, camOri)
%
% oniPath:  string path to .oni file to open
% camOri:   camera orientation ('R'=right, {'H'=horzontal}, or 'L'=left)
%
% see also stream.Sensor

% TODO
% - consider overloading subsref() for easy frame seeking
%       would allow calls like: o = OniFile(file); IR = o(frameNo).getInfrared();
   

%% Properties
    properties (SetAccess = private)
        iFrame = 1          % index of the current frame
        N_FRAMES            % total number of frames collected
        tStamp              % current time stamp in seconds
    end

%% Methods
    methods
    %% Constructor / destructor
        function this = OniFile(oniPath, camOri)
        %% Check inputs and start pre-recorded session
            if nargin > 0
                this.oniPath = oniPath;
                if nargin > 1
                    this.camOri = camOri;
                end
            end
            this.oniPath = checkOniPath_(this.oniPath);
            
            [ID, N] = Mex.mxNiCreateContext_nFramesOut([], this.oniPath);
            assert(N > 0, 'No frames were read!');
            this.streamID_ = ID;
            this.N_FRAMES = N;
        end

    %% Implementations of abstract methods
        function next(this)
        %% Go to next frame if there is one
            this.tStamp = Mex.mxNiUpdateContext_tstampOut(this.streamID_);
            if hasNext(this)
                this.iFrame = this.iFrame + 1;
            else
                fprintf(2, 'LOOPING TO START\n');
                this.iFrame = 1;    % start from the beginning
            end
            drawnow         % FIXME: need this?
        end

        function tf = hasNext(this)
        %% Whether file nas another frame
            tf = this.iFrame < this.N_FRAMES;
        end

        function tf = hasPrev(this)
        %% Whether file nas another frame
            tf = this.iFrame > 1;
        end

    %% Additional methods
        function prev(this)
        %% Try to go to previous frame using seekTo()
            if hasPrev(this)
                seekTo(this, this.iFrame - 1);
            else
                fprintf(2, 'LOOPING TO END\n');
                seekTo(this, this.N_FRAMES);
            end
        end
        
        function seekTo(this, ix)
        %% Go to frame ix if in range
        % NOTE:  special case for ix = 1 or 2 due to issue with xn::Player::seekToFrame()
            N = this.N_FRAMES;
            ID = this.streamID_;
            assert(ix >= 1 && ix <= N, 'exceeded frame range')
            if ix >= 3
                ts = trySeekToFrame_(this, ix);
                % ts = Mex.mxNiSeekToFrame_tstampOut(ID, ix);
            else
                % Either frame 1 or 2 requested, which we cannot seek to.
                % Instead, seek to end of file, then step either 1 or 2 frames
                switch ix
                    case 1
                        trySeekToFrame_(this, N);
                        % Mex.mxNiSeekToFrame_tstampOut(ID, N);
                        ts = Mex.mxNiUpdateContext_tstampOut(ID);
                    case 2
                        trySeekToFrame_(this, N);
                        % Mex.mxNiSeekToFrame_tstampOut(ID, N);
                        Mex.mxNiUpdateContext_tstampOut(ID);
                        ts = Mex.mxNiUpdateContext_tstampOut(ID);
                    otherwise
                        error('the sky is falling!')
                end
            end
            this.tStamp = ts;
            this.iFrame = ix;
        end
        
        function set.tStamp(this, tStamp_us)
        %% Convert time stamp from microseconds to seconds
            this.tStamp = double(tStamp_us) / 1e6;
        end
    end
%% private helper methods
    methods (Access = private)
        function tStamp_ = trySeekToFrame_(this, ix)
        %% Perform seek operation and repeat if error (THIS IS A HACK)
        % FIXME: this is a hack to fix Mex.mxNiSeekToFrame issue
        % - Occasionally (~0.1%), Mex.mxNiSeekToFrame casues frame
        %   desynchronication. This can usually be corrected by repeating 
        %   the call to seekToFrame.
        % - However, sometimes a frame is legitimately missing. Repeated
        %   calls to seekToFrame cannot recover the missing frame. 
            streamID_ = this.streamID_;
            try
                tStamp_ = Mex.mxNiSeekToFrame_tstampOut(streamID_, ix);
                return
            catch Me
                disp(Me.message);
            end
            % give up! step to next frame
            tStamp_ = Mex.mxNiUpdateContext_tstampOut(streamID_, ix);
            this.iFrame = mod(ix, this.N_FRAMES) + 1;   % back to 1 if ix = N_FRAMES 
            warning('COULD NOT SYNC FRAMES. Stepping to next frame');
        end
    end
end

%% helper functions
function oniPath_out = checkOniPath_(oniPath)
%% return full path to oni file (user-supplied if file not found)
    % Ask user to locate if '' or []
    if isempty(oniPath)
        oniPath_out = SelectOniDialog;
        return
    end
    
    % Ensure .oni extension
    [~,~,ext] = fileparts(oniPath);
    if isempty(ext)
        oniPath = [oniPath, '.oni'];
    else
        assert(strcmpi(ext, '.oni'), 'Bad file extension. Should be ".oni"');
    end

    % Check that file exists. If not, ask user to select it manually
    if exist(oniPath, 'file') % oniPath is either:
        % 1) a complete path string which may or may not be on the matlab path
        % 2) the name of a file on the matlab path 
        temp = which(oniPath);
        if isempty(temp)            % case 1
            oniPath_out = oniPath;
        else                        % case 2
            oniPath_out = temp;
        end        
    else
        warning('Could not open the file: %s\n  Please locate using dialogue box',...
            oniPath);
        oniPath_out = SelectOniDialog;
    end
end

function oniPath_out = SelectOniDialog
%% Open dialog allowing user to navigate to .oni file
    [oniPath dirName] = uigetfile('.oni',...
        'SELECT .ONI FILE');
    if ~oniPath
        error('No .oni file selected')
    end
    oniPath_out = fullfile(dirName, oniPath);
end

