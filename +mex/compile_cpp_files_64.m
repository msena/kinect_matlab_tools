function compile_cpp_files_64(OpenNiPath)
% This function compile_cpp_files will compile the c++ code files
% which wraps OpenNI for the Kinect in Matlab.
% 
% modified by Mark Sena 12 March 20123/12/12
%   modified slightly according to notes on MATLAB file exchange to
%   allow for proper compilation on a 64 bit machine. Use the command 
%   compile_cpp_files_64untested('C:\Program Files\OpenNI\')
%
% Please install first on your computer:
% - NITE-Bin-Win32-v1.3.0.18
% - OpenNI-Bin-Win32-v1.0.0.25
%
% Just execute by:
%
%   compile_cpp_files 
%
% or with specifying the OpenNI path
% 
%    compile_cpp_files2('C:\Program Files\OpenNI\')
%
%

% OpenNiPath =getenv('OPEN_NI_INSTALL_PATH64'); 

if(nargin<1)
    OpenNiPathInclude=getenv('OPEN_NI_INCLUDE');
    OpenNiPathLib=getenv('OPEN_NI_LIB64');
    if(isempty(OpenNiPathInclude)||isempty(OpenNiPathLib))
        error('OpenNI path not found, Please call the function like compile_cpp_files(''examplepath\openNI'')');
    end
else
    OpenNiPathLib=[OpenNiPath 'Lib64'];
    OpenNiPathInclude=[OpenNiPath 'Include'];
end

cd +Mex
% files=dir('*.cpp');
files=dir('*.c*');      % also include c files (e.g. hat.c)
for i=1:length(files)
    Filename=files(i).name;
    clear(Filename); 
    mex('-v',['-L' OpenNiPathLib],'-lopenNI64',['-I' OpenNiPathInclude],Filename);
end
cd ..
