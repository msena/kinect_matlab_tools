#include "mex.h"
#include "math.h"
#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>

#define CHECK_RC(nRetVal, what)                   \
  if (nRetVal != XN_STATUS_OK)                  \
  {                               \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return;                       \
  }

// Mark Sena, UCSF, June 2014
// - Wanted to: 1) enable playback and 2) get num frames
// - Added Player node as 6th item in the output MXadress
// - Added 2nd output nNumFrames, the number of frames in the recording
    
//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
xn::Context         g_Context;
xn::IRGenerator     g_IR;
xn::ImageGenerator  g_Image;
xn::UserGenerator   g_User;
xn::DepthGenerator  g_Depth;
// MS____________________________________________________________
xn::Player          g_Player;
// ______________________________________________________________

/* The matlab mex function */
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) {
    XnUInt64 *MXadress;
    // MS____________________________________________________________
    XnUInt32 *MXadress2;    // for 2nd output arg
    // ______________________________________________________________
    XnUInt64 address;
    int argc=0;
    // MS____________________________________________________________
    // int Jdimsc[2]={1,5};  
    int Jdimsc[2]={1,6};    
    int Jdimsc2[2]={1,1};    // size of 2nd output arg is 1-by-1
    // ______________________________________________________________
    char *SAMPLE_XML_PATH;
    char *SAMPLE_DATA_PATH;
    
    // Output the Point to the Kinect Object
    plhs[0] = mxCreateNumericArray(2, Jdimsc, mxUINT64_CLASS, mxREAL);
    // MS____________________________________________________________
    plhs[1] = mxCreateNumericArray(2, Jdimsc2, mxUINT32_CLASS, mxREAL);
    // ______________________________________________________________
  
    if(nrhs==0)  // MS: empty prhs[0] is allowed if a file is supplied
    {
       printf("Open failed: No XML path given \n");
       mexErrMsgTxt("Kinect Error"); 
    }
    
    XnStatus nRetVal = XN_STATUS_OK;
    if (nrhs > 1)  // prerecorded stream
    {
        // MS____________________________________________________________
        if (!mxIsEmpty(prhs[0])) {printf("WARNING: Ignoring 1st input, XML_PATH\n");}
        // ______________________________________________________________

        SAMPLE_DATA_PATH=mxArrayToString(prhs[1]);
         nRetVal = g_Context.Init();
         CHECK_RC(nRetVal, "Init");
         // MS____________________________________________________________
         // nRetVal = g_Context.OpenFileRecording(SAMPLE_DATA_PATH);
          nRetVal = g_Context.OpenFileRecording(SAMPLE_DATA_PATH, g_Player);    // was csFile, hopefully SAMPLE_DATA_PATH is OK
          // ______________________________________________________________
         if (nRetVal != XN_STATUS_OK) {
            printf("Can't open recording %s: %s\n", SAMPLE_DATA_PATH, xnGetStatusString(nRetVal));
            mexErrMsgTxt("Kinect Error"); 
         }
         else
         {
            address=( XnUInt64)&g_Context;
         }
    }
    else        // 1 input -> live stream
    {
        SAMPLE_XML_PATH=mxArrayToString(prhs[0]);
        xn::EnumerationErrors errors;
        nRetVal = g_Context.InitFromXmlFile(SAMPLE_XML_PATH, &errors);
        if (nRetVal == XN_STATUS_NO_NODE_PRESENT) {
            XnChar strError[1024];
            errors.ToString(strError, 1024);
            printf("%s\n", strError);
            mexErrMsgTxt("Kinect Error"); 
        }
        else if (nRetVal != XN_STATUS_OK) {
            printf("Open failed: %s\n", xnGetStatusString(nRetVal));
            mexErrMsgTxt("Kinect Error"); 
        }
        else
        {
            // printf("Open failed: %s\n", xnGetStatusString(nRetVal));
            printf("Start sensor: %s\n", xnGetStatusString(nRetVal)); // MS
            address=( XnUInt64)&g_Context;
        }
    }
 
    MXadress = (XnUInt64*)mxGetData(plhs[0]);
    MXadress[0] = address;
    
    // Detect Image
    nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_Image);
  if (nRetVal != XN_STATUS_OK)                
  { 
        printf("Image Node : Not found\n");
        MXadress[1] = 0;
    }
    else
    {
        printf("Image Node : Found\n");
        MXadress[1] = (XnUInt64)&g_Image;
    }
    
    // Detect Depth
    nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_Depth);
  if (nRetVal != XN_STATUS_OK)                
  { 
        printf("Depth Node : Not found\n");
        MXadress[2] = 0;
    }
    else
    {
        printf("Depth Node : Found\n");
        MXadress[2] = (XnUInt64)&g_Depth;
    }
    
    // Detect Infrared
    nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_IR, g_IR);
    if (nRetVal != XN_STATUS_OK)                
    { 
        printf("Infrared Node :Not found\n");
        MXadress[3] = 0;
    }
    else
    {
        printf("Infrared Node : Found\n");
        MXadress[3] = (XnUInt64)&g_IR;
    }
    
    // Detect User
    nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_User);
  if (nRetVal != XN_STATUS_OK)
  {
    nRetVal = g_User.Create(g_Context);
  }
        
    if (nRetVal != XN_STATUS_OK)                
    { 
         printf("User Node : Not found\n");
         MXadress[4] = 0;
    }
    else
    {
        printf("User Node : Found\n");
        MXadress[4] = (XnUInt64)&g_User;
    }
    
    // MS____________________________________________________________
    // Detect Player node
    nRetVal = g_Context.FindExistingNode(XN_NODE_TYPE_PLAYER, g_Player);
    if (nRetVal != XN_STATUS_OK)                
    { 
        printf("Player Node : Not found\n");
        MXadress[5] = 0;
    }
    else
    {
        printf("Player Node : Found\n");
        MXadress[5] = (XnUInt64)&g_Player;

        // Maximum playback speed
        g_Player.SetPlaybackSpeed( XN_PLAYBACK_SPEED_FASTEST );
        XnDouble fSpeed = g_Player.GetPlaybackSpeed();
        printf("Playback speed : %f\n", fSpeed);

    }

    // get number of frames as 2nd output argument. 0 if failed.
    MXadress2 = (XnUInt32*)mxGetData(plhs[1]);
    MXadress2[0] = 0;
    if (g_Player.IsValid()) {
        const XnChar* strNodeName = NULL;
        if (g_Depth.IsValid()) {
            strNodeName = g_Depth.GetName();

            // get the number of frames
            XnUInt32 nNumFrames = 0;
            nRetVal = g_Player.GetNumFrames(strNodeName, nNumFrames);
            if (nRetVal != XN_STATUS_OK)
            {
                printf("Failed to get number of frames: %s\n", xnGetStatusString(nRetVal));
                mexErrMsgTxt("Kinect Error");
            }
            else 
            {
                printf("Loaded %s\n ...with %i frames\n", SAMPLE_DATA_PATH, nNumFrames);
                MXadress2[0] = nNumFrames;
            }
        } else { printf("Invalid Depth\n"); mexErrMsgTxt("Kinect Error"); }
    }   
    // ______________________________________________________________
   
            
    nRetVal = g_Context.StartGeneratingAll();
    CHECK_RC(nRetVal, "StartGenerating");
    // Read next available data
    g_Context.WaitAndUpdateAll();
}
