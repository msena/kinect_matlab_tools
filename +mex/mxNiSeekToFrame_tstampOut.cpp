#include "mex.h"
#include "math.h"
#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>

#define CHECK_RC(nRetVal, what)                   \
  if (nRetVal != XN_STATUS_OK)                  \
  {                               \
    printf("%s failed: %s\n", what, xnGetStatusString(nRetVal));\
    return;                       \
  }
    
// mxNiSeekToFrame(Kinect_handles, nMyFrame) seeks to frame nFrame
// - throws an error if the frames go out of sync 
//      (can sometimes be resolved if this function call is repeated)
// - throws an error if cannot seek to nMyFrame
// - nFrame must be bewteen 3 and the total number of frames (inclusive)
// Mark Sena, UCSF, June 2014
//
// NOTES:
// - wanted ability to go to any frame in the OniFile
// - looked up frame seeking code in Device.cpp from NiViewer sample
// - learned that pre-OpenNi v1.5.7 seekToFrame() would screw up synchronization between Nodes
// - found a fix:  call WaitAndUpdateAll() 3 times after calling seekToFrame()
//      this ensures that IR and Depth frames are in sync.
//      However, frames 1 and 2 cannot be accessed due to the 3x update
// - this should be used in conjunction with mxNiCreateContext_iFrameOut.cpp
//      which creates a Player node for pre-recorded .oni files
//
// TODO:
// - better check on inputs (valid pointer and integer frame no.)

//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
xn::Context         g_Context;
xn::DepthGenerator  g_Depth;
xn::IRGenerator     g_IR;
xn::Player          g_Player;

/* The matlab mex function */
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) 
{
// check inputs
    // TODO: check for invalid pointer somehow?
    if (nrhs < 2 || mxIsEmpty(prhs[0])) {
       printf("Seek failed: Give Pointer and nMyFrame to Kinect as input\n");
       mexErrMsgTxt("Kinect Error"); 
    }

    // input 1: Sensor contexts
    // XnUInt64 *MXaddress;
    // MXaddress = (XnUInt64*)mxGetData(prhs[0]);
    XnUInt64 * MXaddress = (XnUInt64*)mxGetData(prhs[0]);     // todo: does this work?

    // input 1: desired frame number to seek to
    double dFrame = mxGetScalar(prhs[1]);
    int nMyFrame = (int)dFrame;

// init output pointer MXout for timestamp
    int Jdimsc[2]={1,1};        
    plhs[0] = mxCreateNumericArray(2, Jdimsc, mxUINT64_CLASS, mxREAL);
    XnUInt64 * MXout = (XnUInt64*)mxGetData(plhs[0]);
    XnUInt64 nTimestamp;

// init requred nodes
    // context
    if (MXaddress[0]>0) { g_Context = ((xn::Context*) MXaddress[0])[0]; }
    else { mexErrMsgTxt("No Kinect Context"); }

    // Player
    if (MXaddress[5]>0) { g_Player = ((xn::Player*) MXaddress[5])[0]; }
    else { mexErrMsgTxt("No Player node in Kinect Context"); }

    // Depth
    if (MXaddress[2]>0) { g_Depth = ((xn::DepthGenerator*) MXaddress[2])[0]; }
    else { mexErrMsgTxt("No Depth node in Kinect Context"); }

    // Infrared
    if (MXaddress[3]>0) { g_IR = ((xn::IRGenerator*) MXaddress[3])[0]; }
    else { mexErrMsgTxt("No Infrared node in Kinect Context"); }

// seek Depth to frame 'nMyFrame'
    const XnChar* strNodeName = NULL;
    XnStatus nRetVal = XN_STATUS_OK;
    
    // get node name, then seek
    if (g_Depth.IsValid()) {
        strNodeName = g_Depth.GetName();
        nRetVal = g_Player.SeekToFrame(
            strNodeName,                    // Depth
            nMyFrame - 2,                   // correct for extra update**
            XN_PLAYER_SEEK_SET);
        CHECK_RC(nRetVal, "SeekToFrame");
    }
    else { mexErrMsgTxt("Depth node is invalid"); }

    // Update context 3 TIMES!!!**
    //  2nd and 3rd updates fixes frame lag between Depth and other nodes
    g_Context.WaitAndUpdateAll();
    g_Context.WaitAndUpdateAll();
    g_Context.WaitAndUpdateAll();

// check that frames are in sync between depth and IR
    XnUInt32 nDepthFrame = 0;
    XnUInt32 nIRFrame = 0;
    
    // Depth frame no.
    nRetVal = g_Player.TellFrame(strNodeName, nDepthFrame);
    CHECK_RC(nRetVal, "TellFrame(Depth)");
    if (nRetVal == XN_STATUS_OK) { printf("Seeked %s to frame \t%u", strNodeName, nDepthFrame); }

    // Infrared frame no.
    if (g_IR.IsValid()) {
        strNodeName = g_IR.GetName();
        nRetVal = g_Player.TellFrame(strNodeName, nIRFrame);
        CHECK_RC(nRetVal, "TellFrame(IR)");
        if (nRetVal == XN_STATUS_OK) { printf(" (%s: %u)\n", strNodeName, nIRFrame); }
    }
    else { mexErrMsgTxt("Infrared node is invalid"); }

    // warning if IR and Depth out of sync
    if (nDepthFrame != nIRFrame) { 
        printf("********\nmxNiSeekToFrame::Frames out of sync!\n\t%i(Depth), %i(IR)\n********\n",
            nDepthFrame, nIRFrame);
        mexErrMsgTxt("mxNiSeekToFrame::Frames out of sync!");
    }

    // throw error if either Depth frame not as requested
    if (nMyFrame != nDepthFrame) { 
        printf("Could not seek to : %i\n\t%i(Depth), %i(IR)\n",
            nMyFrame, nDepthFrame, nIRFrame);
        mexErrMsgTxt("Could not seek!");
    }

// output the timestamp.
    nRetVal = g_Player.TellTimestamp(nTimestamp);
    CHECK_RC(nRetVal, "TellTimestamp(Player)");
    MXout[0] = nTimestamp;
}