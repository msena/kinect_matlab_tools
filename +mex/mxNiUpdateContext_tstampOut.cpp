#include "mex.h"
#include "math.h"
#include <XnOpenNI.h>
#include <XnCodecIDs.h>
#include <XnCppWrapper.h>

// mxNiUPdateContext updates the context and outputs timestamp
// - gives warning if frames go out of sync 
//   (which happens rarely and cannot be resolved to my knowledge)
// - Valid only for pre-recorded streams with valid Player node
// - Use mxNiUpdateContext.cpp for live playback
// Mark Sena, UCSF, June 2014

// TODO:
// - check that a MXaddress input of size 5 instead of 6 doesnt break program
    
//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
xn::Context         g_Context;
xn::Player          g_Player;
xn::DepthGenerator  g_Depth;    // for debugging frame number only
xn::IRGenerator     g_IR;       // for debugging frame number only

/* The matlab mex function */
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) {
    // check input
    XnUInt64 * MXaddress;
    if (nrhs==0 || mxIsEmpty(prhs[0])) {
        printf("Update failed: Give Pointer to Kinect as input\n");
        mexErrMsgTxt("Kinect Error"); 
    }
    MXaddress = (XnUInt64*)mxGetData(prhs[0]);

    // init output pointer MXout for timestamp
    int Jdimsc[2]={1,1};        
    plhs[0] = mxCreateNumericArray(2, Jdimsc, mxUINT64_CLASS, mxREAL);
    XnUInt64 * MXout = (XnUInt64*)mxGetData(plhs[0]);
    XnUInt64 nTimestamp;
    XnStatus nRetVal = XN_STATUS_OK;

    // Read next available data
    if (MXaddress[0]>0){ g_Context = ((xn::Context*) MXaddress[0])[0]; }
    g_Context.WaitAndUpdateAll();

    // get pointers to player node
    if (MXaddress[5]>0) { g_Player = ((xn::Player*) MXaddress[5])[0]; }
    else { mexErrMsgTxt("No Player node in Kinect Context"); }

    // output timestamp
    nRetVal = g_Player.TellTimestamp(nTimestamp);
    if (nRetVal != XN_STATUS_OK) {printf("Failed to tell timestamp\n");}
    MXout[0] = nTimestamp;  


// print frame numbers (debugging)
    const XnChar* strNodeName = NULL;
    XnUInt32 nDepthFrame = 0;
    XnUInt32 nIRFrame = 0;
    if (MXaddress[2]>0) { g_Depth = ((xn::DepthGenerator*) MXaddress[2])[0]; }
    if (MXaddress[3]>0) { g_IR = ((xn::IRGenerator*) MXaddress[3])[0]; }

    // print depth frame number
    if (g_Depth.IsValid()) {
        strNodeName = g_Depth.GetName();
        nRetVal = g_Player.TellFrame(strNodeName, nDepthFrame);
        if (nRetVal == XN_STATUS_OK) { printf("Stepped %s to frame\t%u", strNodeName, nDepthFrame); }
    }
    else { mexErrMsgTxt("Depth node is invalid"); }

    // print IR frame number
    if (g_IR.IsValid()) {
        strNodeName = g_IR.GetName();
        nRetVal = g_Player.TellFrame(strNodeName, nIRFrame);
        if (nRetVal == XN_STATUS_OK) { printf(" (%s: %u)\n", strNodeName, nIRFrame); }
    }
    else { mexErrMsgTxt("IR node is invalid"); }

    // warning if IR and Depth out of sync
    if (nDepthFrame != nIRFrame) { 
        printf("mxNiUpdateContext::Frames out of sync!\n\t%i(Depth), %i(IR)\n",
            nDepthFrame, nIRFrame);
    }
}
