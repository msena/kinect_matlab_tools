% This function mxNiStopCapture, this function closes the OpenNI recorder
% and stops capturing of frames
%
% mxNiStopCapture(CaptureHandle);
%
% inputs,
%   CaptureHandle : A handle to a OpenNI recorder
%
% See also mxNiCreateContext, mxNiUpdateContext, mxNiDepth, mxNiPhoto,
%		mxNiInfrared, mxNiSkeleton, compile_cpp_files
%
% Mex-Wrapper is written by D.Kroon University of Twente (March 2011) 
%
% NOTE: I found that recording continues after mxNiStopCapture is called.
% To stop recording, one must use mxNiDeleteContext.
