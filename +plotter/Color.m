classdef Color < plotter.IPlotter
%% Displays color data in RGB format

methods
    function this = Color(varargin)
        this = this@plotter.IPlotter(varargin{:});
    end
end

methods (Access = protected)
%% Used internally by IPlotter
    function initPlot_(this)
    %% Create a plot of the current dta
        this.h_plot = imshow(this.data);
    end

    function updatePlot_(this)
    %% Update graphics handles with new data
        set(this.h_plot, 'cdata', this.data);
    end
end

end % classdef