classdef Skel < plotter.IPlotter
%% Display skeleton data with colored limbs (supports overlay)
%
% The skeleton data structure is a (nUsers*15)-by-7 matrix.
% Each row maps to a particular joint as follows:
%
%   Index       Joint
%   -----       -----
%   1,2         head, neck, 
%   3,4,5       shoulderL, elbowL, handL, 
%   6,7,8       shoulderR, elbowR, handR, 
%   9           torso, 
%   10,11,12    hipL, kneeL, footL, 
%   13,14,15    hipR, kneeR, footR

%% Properties
properties (Constant, Hidden)
    % chain     joints      base
    I_HEAD =    [1          2];
    I_ARM_L =   [5 4 3      2];
    I_ARM_R =   [8 7 6      2];
    I_TORSO =   [9          2];
    I_LEG_L =   [12 11 10   9];
    I_LEG_R =   [15 14 13   9];  
end

%% Methods
methods
    function this = Skel(varargin)
        this = this@plotter.IPlotter(varargin{:});
    end
end

methods (Access = protected)
%% Methods used internally by IPlotter
    function initPlot_(this)
        %% Create a line plot of the skeleton data

        x = this.data(:,1);
        y = this.data(:,2);
        z = this.data(:,3);

        % plot lines connecting joints
        lnWd = 2;
        h(1) = line( x(this.I_HEAD),  y(this.I_HEAD),  z(this.I_HEAD),  'color', 'c', 'linewidth', lnWd );
        h(2) = line( x(this.I_ARM_L), y(this.I_ARM_L), z(this.I_ARM_L), 'color', 'r', 'linewidth', lnWd );
        h(3) = line( x(this.I_ARM_R), y(this.I_ARM_R), z(this.I_ARM_R), 'color', 'b', 'linewidth', lnWd );
        h(4) = line( x(this.I_TORSO), y(this.I_TORSO), z(this.I_TORSO), 'color', 'g', 'linewidth', lnWd );
        h(5) = line( x(this.I_LEG_L), y(this.I_LEG_L), z(this.I_LEG_L), 'color', 'm', 'linewidth', lnWd );
        h(6) = line( x(this.I_LEG_R), y(this.I_LEG_R), z(this.I_LEG_R), 'color', 'c', 'linewidth', lnWd );
        
        axis equal
        this.h_plot = h;
    end
    
    function updatePlot_(this)
        %% Update graphics handles with new data
        x = this.data(:,1);
        y = this.data(:,2);
        z = this.data(:,3);
        h = this.h_plot;
        
        set(h,...
            {'xdata'},  {
                x(this.I_HEAD)
                x(this.I_ARM_L)
                x(this.I_ARM_R)
                x(this.I_TORSO)
                x(this.I_LEG_L)
                x(this.I_LEG_R) },...
            {'ydata'},  {
                y(this.I_HEAD)
                y(this.I_ARM_L)
                y(this.I_ARM_R)
                y(this.I_TORSO)
                y(this.I_LEG_L)
                y(this.I_LEG_R) },...
            {'zdata'},  {
                z(this.I_HEAD)
                z(this.I_ARM_L)
                z(this.I_ARM_R)
                z(this.I_TORSO)
                z(this.I_LEG_L)
                z(this.I_LEG_R) } );
    end
end

end % classdef