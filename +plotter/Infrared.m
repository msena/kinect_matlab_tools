classdef Infrared < plotter.IPlotter
%% Displays infrared data with gray/blue/magenta colormap

methods
    function this = Infrared(varargin)
        this = this@plotter.IPlotter(varargin{:});
    end
end

methods (Access = protected)
%% Used internally by IPlotter
    function initPlot_(this)
    %% Plot an image with 25% brightest pixels colored cyan/magenta
        this.h_plot = imshow(this.data,[]);
        colormap([gray(192); cool(64)]);
    end

    function updatePlot_(this)
    %% Update graphics handles with new data
        set(this.h_plot, 'cdata', this.data);
    end
end

end % classdef