classdef Depth < plotter.IPlotter
%% Display depth data with gray colormap scaled between 1.5m and 2.5m

methods
    function this = Depth(varargin)
        this = this@plotter.IPlotter(varargin{:});
    end
end

methods (Access = protected)
%% Used internally by IPlotter
    function initPlot_(this, depth_range)
    %% Create a depth image and scale colormap within depth_range
        if nargin < 2,  depth_range = [1500, 2500]; end
        this.h_plot = imshow(this.data,...
            'displayrange',     depth_range,...
            'colormap',         gray);
    end

    function updatePlot_(this)
    %% Update graphics handles with new data
        set(this.h_plot,...
            'cdata',    this.data);
    end
end

end % classdef