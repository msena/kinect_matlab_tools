classdef Points < plotter.IPlotter
%% Displays 3D pointcloud data with optional color.

% TODO:
% - fix camOri L and R views
% - use hgtransform to change view instead of a rotation matrix on the data

methods
%% Public
    function this = Points(varargin)
        this = this@plotter.IPlotter(varargin{:});
    end
end

methods (Access = protected)
%% Used internally by IPlotter
    function initPlot_(this, myColor)
    %% Create a plot of pointcloud data with optional color
        if nargin < 2, myColor = 'k'; end
        p = this.data;
        this.h_plot = plot3( p(:,1), p(:,2), p(:,3),...
            'marker',       '.',...
            'markersize',   1,...
            'linestyle',    'none',...
            'color',        myColor...
            );
            axis equal
            view(3)
    end

    function updatePlot_(this)
    %% Update graphics handles with new data
        p = this.data;
        set(this.h_plot,...
            'xdata',    p(:,1),...
            'ydata',    p(:,2),...
            'zdata',    p(:,3));
    end
end

end % classdef