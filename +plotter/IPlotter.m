classdef (Abstract) IPlotter < handle & matlab.mixin.Heterogeneous
%% (Abstract) Classes for plotting and processing data
% - create and update plots for a specified data type
% - apply a processing function to input data
% - define a hierarchy of IPlotter nodes for multi-step processing
%
%   P = plotter.<IPlotter subclass>(input_data)
%   P = plotter.<IPlotter subclass>(input_data, function_handle)

% TODO
% - decide if h_axes needs to be a property
% - decide if f_process should be modifiable after construction

%% Properties
properties  % publicly modifyable
    params                      % Parameters for function f_process()
end

properties (SetAccess = protected)
    inputs                      % Data to be operated on by f_process()
    f_process = ...             % Processing function (return inputs by default)
        @(inps, params) inps;
    h_plot                      % Handle to graphics objects
end

properties (SetAccess = private)
    data                        % Internal representation of processed data
end

%% Methods
methods (Abstract, Access = protected)
%% Must be implemented by subclasses, and for internal use only
    initPlot_(this, varargin)   % Create this.h_plot given this.data
    updatePlot_(this)           % Set this.h_plot given this.data
end

methods
%% Public
    function this = IPlotter(inputs, f_process, params)
    %% Set the input data and optional processing function
        this.inputs = inputs;
        if nargin > 1
            this.f_process = f_process;
            if nargin > 2
                this.params = params;
            end
        end
        update(this);
    end

    function set.params(this, params)
    %% Automatically update plotter upon changing parameters
        this.params = params;
        update(this);
    end

    function set.f_process(this, f)
    %% Ensure that f_process is a valid function handle with 2 inputs
        assert( isa(f, 'function_handle'),...
            'f_process must be a function handle' );
        assert( nargin(f) == 2,...
            'f_process must have 2 arguments. The 2nd may be a ~' );
        this.f_process = f;
    end
end

methods (Sealed)
%% Cannot be re-implemented by subclasses
    function plot(these, varargin)
    %% Update and create plot for each of 'these' if there's data
        update(these);       % Ensure up to date before plotting
        for this = these(:)'                
            if ~isempty(this.data)
                initPlot_(this, varargin{:})
            end
        end
    end
    
    function update(these)
    %% Recursively update inputs, process data, and update plot
        for this = these(:)'
            % recursively process data
            inps = this.inputs;
            if isa(inps, 'plotter.IPlotter')
                update(inps);
            end
            this.data = this.f_process(inps, this.params);
            
            % try to update the plot if it has been initialized
            if ishandle(this.h_plot)
                try
                    updatePlot_(this)
                    drawnow
                catch Me
                    disp(['DID NOT UPDATE PLOT: ', Me.message])
                end
            end
        end
    end 
end

end % classdef