classdef (Abstract) IViewer < handle

%% Properties
properties (SetAccess = protected)
    h_figure
    Stream
    isStopped = true
end

properties (SetAccess = private, Hidden)
    plotterList_ = {}
    iPlotter_ = 1           % index of the active plotter
    keyList_ = {}
    fcnList_ = {}
end

properties(Dependent)
    currPlotter             % the currently selected plotter
end

properties (Dependent, Hidden)
    nPlotters_
end

%% Methods
methods
%% constructor / desructor
    function this = IViewer()
    %% Create figure and initialize 1st plotter
        this.h_figure = figure(...  
            'NumberTitle',          'off',...
            'Name',                 class(this),...
            'WindowKeyPressFcn',    @this.keyCallback_,...
            'CloseRequestFcn',      @this.closeFigCallback_,... 
            'toolbar',              'figure'...  % keep toolbar available
            );
        
        % Up/down arrow keys cycle through frames
        this.keyList_{1} = 'uparrow';
        this.fcnList_{1} = @this.nextPlotter_;
        this.keyList_{2} = 'downarrow';
        this.fcnList_{2} = @this.prevPlotter_;

    end

    function delete(this)
    %% Stop streams and close figures
    % - TODO: give user option to save data
        delete(this.Stream);
    	delete(this.h_figure);
    end

    function n = get.nPlotters_(this)
    %% Number of plotters
        n = length(this.plotterList_);
    end
    
    function P = get.currPlotter(this)
    %% Currently selected plotter
        P = this.plotterList_{this.iPlotter_};
    end
end

methods (Access = protected)
%% For use by subclasses
    function addPlotter(this, varargin)
    %% Add plotters to the list
        for i = 1:length(varargin)
            Plotter = varargin{i};
            assert(isa(Plotter, 'plotter.IPlotter'),...
                'inputs must be Plotter objects');
            this.plotterList_ = [this.plotterList_, {Plotter}];
        end
    end

    function addKeyPress(this, varargin)
    %% Add callback functions to execute upon pressing key
    % SYNTAX: viewerObj.addKeyPress(key, callback_func,...)
    % - callbacks cannot take any input arguments
        nIn = length(varargin);
        assert(~mod(nIn, 2),...
            'not enough inputs. Provide key-function pairs.');
        for i  = 1:2:nIn
            key = varargin{i};
            f_callback = varargin{i+1};
            
            % validate inputs:
            %   key is a char (not 'uparrow'/'downarrow')
            %   f_callback is a function_handle
            assert(ischar(key),...
                'key must be a char array');
            assert(~any(strcmpi(key, {'uparrow', 'downarrow'})),...
                'uparrow and downarrow are reserved for Plotter cycling');
            this.keyList_ = [this.keyList_, {key}];
            
            assert(isa(f_callback, 'function_handle'),...
                'f_callback must be a function handle');
            this.fcnList_ = [this.fcnList_, {f_callback}];
        end
    end

    function dispCont(this)
    %% Continuously display next until figure closed, viewer stopped, or read end of file
        while isvalid(this) && ishandle(this.h_figure)...
                            && ~this.isStopped...
%                             && hasNext(this.Stream)
            dispNext(this)
            drawnow
        end
    end

    function dispNext(this)
    %% Step to and plot the next frame
        next(this.Stream);
        update(this.currPlotter);
    end
end

methods (Access = private)
%% Helper methods
    function keyCallback_(this, ~, event)
    %% select callbacks from list and execute them
    % - multiple callbacks for the same key will be executed in the order added
        key = event.Key;
        for ix = find( strcmpi(key, this.keyList_) )
            f = this.fcnList_{ix};
            fprintf( 'Running callback %d for key [%s]: %s\n',...
                ix, key, funToStr(f) );
            f()  % execute function handle f with 0 inputs
        end
    end

    function nextPlotter_(this)
    %% Inititialize and update next plotter
        this.iPlotter_ = min(this.iPlotter_ + 1, this.nPlotters_);
        plot(this.currPlotter);
    end

    function prevPlotter_(this)
    %% Inititialize and update previous plotter
        this.iPlotter_ = max(1, this.iPlotter_ - 1);
        plot(this.currPlotter);
    end

    function closeFigCallback_(this, ~, ~)
    %% Delete the viewer (see destructor)
        delete(this)
    end
end

end % classdef

%% helper functions
    function str = funToStr(f_handle)
    %% convert function handle to readable string representation
    	str = char(f_handle);
        str = strrep(str, 'varargin', ' ');
        str = strrep(str, '{:}', '');
        str = strrep(str, '()', '');
    end
