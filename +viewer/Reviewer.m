classdef Reviewer < viewer.IViewer

methods
    function this = Reviewer(oniPath, camOri)
    %% Contructor: Start stream. Add plotters and callbacks
        this = this@viewer.IViewer();      % superclass constructor
        
        % Start stream from oni file
        if nargin < 2
            camOri = [];
            if nargin < 1
                oniPath = [];
            end
        end
        S = stream.OniFile(oniPath, camOri);
        this.Stream = S;

        % Add callbacks for frame navigation
        addKeyPress(this,...
            'rightarrow',   @this.dispNext,...  % dispNext() defined in IViewer
            'leftarrow',    @this.dispPrev_,...
            'space',        @this.playPause_...
            )

        % Define list of Plotters and their processing functions,
        % which obtain data from the Stream
        addPlotter(this,...
            plotter.Infrared(   S, @(S,~)S.getInfrared),...
            [plotter.Depth(     S, @(S,~)S.getDepth),...
                plotter.Skel(       S, @(S,~)S.getSkel2D)],...
            [plotter.Points(    S, @(S,~)S.getPoints),...
                plotter.Skel(       S, @(S,~)S.getSkel3D)]...
            );
        plot(this.currPlotter);     % initialize the first
    end
end

methods (Access = private)
%% Private helper methods
    function dispPrev_(this)
    %% Display previous frame
        prev(this.Stream);
        update(this.currPlotter);
    end

    function playPause_(this)
    %% Start or stop continuous display
        if this.isStopped
            this.isStopped = false;
            fprintf(2,'PLAY\n');
            dispCont(this);
        else
            this.isStopped = true;
            fprintf(2,'PAUSED\n');
        end
    end
end

end % classdef
