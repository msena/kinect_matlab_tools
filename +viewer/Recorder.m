classdef Recorder < viewer.IViewer

% TODO
% - consider keeping oniPath in Sensor object...
% - allow user to change camera orientation 
% - ability to preload file name list
% - consider locking window

%% Properties
properties (SetAccess = private, Hidden)
    h_fileNameBox_      % where user enters name of file to save
    dirPath_ = pwd      % path to the saving directory
end

properties (Dependent, Hidden)
    oniName_            % name of .oni file to save (not including path)
end

%% Methods
methods
    function this = Recorder(camOri)
    %% Start stream. Add plotters and callbacks
        
        % Start sensor stream
        if nargin < 1
            camOri = [];
        end
        S = stream.Sensor(camOri);
        this.Stream = S;

        % add UI control and callback for start/stop recording
        addUIControl_(this);
        addKeyPress(this,...
            'space',   	@this.recStartStop_...
            );
        
        % Define list of Plotters and their processing functions,
        % which obtain data from the Stream
        addPlotter(this,...
            plotter.Infrared(   S, @(S,~)S.getInfrared),...
            plotter.Depth(      S, @(S,~)S.getDepth),...
            plotter.Points(     S, @(S,~)S.getPoints)...
            );
        plot(this.currPlotter);     % initialize the first

        % start streaming immediately
        this.isStopped = false;
        dispCont(this);
    end

%% getters and setters
    % ensure the oniName_ and fileNameBox correspond
    function fName = get.oniName_(this)
    %% retreive file name from input box
    % - ignore spaces
    % - use date as the default name
        fName = get(this.h_fileNameBox_, 'string');
        fName = strrep(fName, ' ', '');
        if isempty(fName)
            fName = ['k_', datestr(date)];
        end
    end

end
methods (Access = private)
%% Private helper methods
    function addUIControl_(this)
    %% Add filename text box and browse-directory button
        
        % text box for entering name of file to save
        posBox = [10 10 100 20];  % [left bottom width height]
        this.h_fileNameBox_ = uicontrol('style', 'edit',...
            'string',               '',...  % file name
            'position',             posBox,...
            'HorizontalAlignment',  'right',...
            'TooltipString',        'name of file to save'...
            );
        posTxt = posBox + [0 posBox(4) 0 0];  % above box
        uicontrol('style', 'text',...               % textbox label
            'string',               'File name:',...
            'position',             posTxt,... 
            'HorizontalAlignment',  'left',...
            'BackgroundColor',      [.8 .8 .8]...   % same as fig background
            );

        % button to browse for directory in which to save
        posDir = posBox + [posBox(3) 0 0 0];  % right of box
        uicontrol('style', 'pushbutton',...
            'string',               'Select folder',...
            'callback',             @this.selectDirectory_,...
            'position',             posDir,...
            'HorizontalAlignment',  'left',...
            'TooltipString',        'location of saving directory'...
            );
    end

    function selectDirectory_(this,~,~)
    %% Choose saving file directory and pre-pend to file name
        dirPath = uigetdir;
        if dirPath
            this.dirPath_ = dirPath;
        end
    end

    function recStartStop_(this)
    %% Start or stop recording if not currently displaying Points
        isaPointsObj = arrayfun( @(P)isa(P, 'plotter.Points'),...
            this.currPlotter);
        if ~any(isaPointsObj)
            if this.Stream.isRecording 
                title(gca, '\bfDONE RECORDING', 'color', 'k');
                recStop(this.Stream);
            else
                oniPath = fullfile(this.dirPath_, this.oniName_);
                oniPath = checkOniPath_(oniPath);
                recStart(this.Stream, oniPath);
                title(gca, '\bfRECORDING', 'color', 'r');
            end
        else
            fprintf(2,'CANT RECORD WHILE VIEWING POINTS\n');
        end
    end
end

end % classdef

%% helper functions
    function filePath = checkOniPath_(filePath)
    %% If file exists append _##. Overwrite only if user confirms
    % - ignore extensions (.oni added later)
    % - throw error if empty file name
        
        % Ensure .oni extension and non-empty file name
        [fPath, fName, ext] = fileparts(filePath);
        if ext, warning(['ignoring extension: ', ext]), end
        filePath = [fullfile(fPath, fName), '.oni'];     % reassemble path
        assert(~isempty(fName), 'empty file name')
        
        % either append _## suffix to or overwrite an existing file
        while exist(filePath, 'file')
            % append _## suffix. Ask user to confirm/replace file name
            defaultName = append_NN(fName);
            answer = inputdlg(...
                'File already exists. Enter new file name:',...
                '', 1, {defaultName});
            assert(~isempty(answer), 'NOT RECORDING. REQUEST CANCELLED')
            fName = answer{1};

            % if user-supplied file still exists, confirm overwrite
            filePath = [fullfile(fPath, fName), '.oni'];     % reassemble path
            if exist(filePath, 'file')
                response = questdlg('File already exits. Overwrite?', ...
                    '', 'Yes', 'No', 'No');
                if strcmpi(response, 'Yes')
                    disp(['overwriting file ', fName])
                    break
                end
            end
        end
    end

    function fName = append_NN(fName)
    %% append _## suffix to fileName
        if length(fName)>2 && any(regexp(fName(end-2:end),'_\d\d'))
            % fName has a _## suffix. Increment it by 1
            suffix = fName(end-1:end);                  % ##
            fName = fName(1:end-3);
            temp = num2str( 100 + str2double(suffix) + 1 );
            suffix = temp(end-1:end);                   % ##+1
        else % fName has no suffix. Append _01
            suffix = '01';
        end
        fName = [fName,'_',suffix];
    end